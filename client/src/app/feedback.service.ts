import { Injectable } from '@angular/core';
import {Http,RequestOptions,Headers} from '@angular/http';

@Injectable()
export class FeedbackService {
    
url="http://localhost:3000/feedback";
http:Http;
    constructor(http:Http) {
        this.http=http;
        
     }
     getFeedback()
     {
         return this.http.get(this.url);
     }

     saveFeedbackService(review,name,email,comments)
     {
         var body={
            
                "review":review,
                "name":name,
                "email":email,
                "comments":comments            

         };
         var header=new Headers({'Content-type':'application/json'});
         var requestOption=new RequestOptions({headers:header});
        return this.http.post(this.url,body,requestOption);
         
     }

}