import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class EmployeeService {

  url = 'http://localhost:3000/employee';

  constructor(private http: Http) {

  }

  getEmployee() {
    return this.http.get(this.url);
  }

  getEmployeeDetails(employeeId: number) {
    return this.http.get(this.url + '/' + employeeId);
  }

  addEmployee(password:String, empFirstName:String, empLastName:String, role:String, address:String, empUID:String, dateOfJoining:String, mobileNo:String, emailId:String) {

      const body = {
        password:password,
        empFirstName:empFirstName,
        empLastName:empLastName, 
        role:role, 
        address:address, 
        empUID:empUID, //this.service.updateInventoryItem(productId);
        dateOfJoining:dateOfJoining, 
        mobileNo:mobileNo, 
        emailId:emailId
      };

      const header = new Headers({ 'Content-Type': 'application/json' });
      const requestOption = new RequestOptions({ headers: header });

      return this.http.post(this.url, body, requestOption);
  }

  updateEmployee(password:String, empFirstName:String, empLastName:String, role:String, address:String, empUID:String, dateOfJoining:String, mobileNo:String, emailId:String) {

    const body = {
      password:password,
      empFirstName:empFirstName,
      empLastName:empLastName, 
      role:role, 
      address:address, 
      empUID:empUID, 
      dateOfJoining:dateOfJoining, 
      mobileNo:mobileNo, 
      emailId:emailId
    };

    const header = new Headers({ 'Content-Type': 'application/json' });
    const requestOption = new RequestOptions({ headers: header });

    return this.http.put(this.url, body, requestOption);
}

  deleteEmployee(employeeId: number) {
    return this.http.delete(this.url + '/' + employeeId);
  }

}
