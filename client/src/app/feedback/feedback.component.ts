import { Component, OnInit } from '@angular/core';
import { FeedbackService } from '../feedback.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
feedbacks =[];
name:string;
review:string;
email:string;
comments:string;
service:FeedbackService;
isVisible:number=0;
  constructor(service:FeedbackService) {
    this.service=service;
   this.getAllFeedback();
   }
   getAllFeedback()
   {
    this.service.getFeedback().subscribe((response)=>{
      var result=response.json();
      console.log(result);
      if(result!=null)
      {
        this.feedbacks=result.data;
    
      }
      else
      {
        alert("OOPS something went wrong");
      }

    });

   }

   saveFeedback()
   {

    this.service.saveFeedbackService(this.review,this.name,this.email,this.comments).subscribe((response)=>{
      var result=response.json();
      if(result!=null)
      {
        alert("successfully saved");
      }

      else
      {
        alert("something went wrong");
      }
    });
    
  }
  ngOnInit() {
  }

}