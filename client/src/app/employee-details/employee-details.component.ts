import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {
  selectedEmployee = {};

  constructor(
    private employeeService: EmployeeService,
    private activedRoute: ActivatedRoute) {

    this.activedRoute.queryParams.subscribe(params => {
      const employeeId = params['employeeId'];

      this.employeeService
        .getEmployeeDetails(employeeId)
        .subscribe(response => {
          const result = response.json();
          this.selectedEmployee = result.data;
        });
    });
  }

  ngOnInit() {
  }

}
