import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'list-customer',
  templateUrl: './list-customer.component.html',
  styleUrls: ['./list-customer.component.css']
})
export class ListCustomerComponent implements OnInit {
  Customers = [];
  custFirstName ='';
  custLastName =''; 
  address =''; 
  custMobile =''; 
  custEmail ='';
  custPan = '';

  constructor(
    private router: Router,
    private customerService: CustomerService) {
      this.refreshCustomerList();
     }


  ngOnInit() {
  }

  refreshCustomerList() {
    this.customerService
      .getCustomer()
      .subscribe(response => {
        const result = response.json();
        console.log(result);
        this.Customers = result.data;
      });
  }
  onAdd() {

    this.customerService
      .addCustomer(this.custFirstName, this.custLastName, this.custMobile, this.custEmail, this.custPan)
      .subscribe(response => {
        console.log(response);
        this.router.navigate(['/list-customer']);
      });
  }
}
