import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {

  movies = [];

  // dependancy injection
  constructor(
    private router: Router,
    private movieService: MovieService) {

    this.refreshMovieList();
  }

  refreshMovieList() {
    this.movieService
      .getMovies()
      .subscribe(response => {
        const result = response.json();
        console.log(result);
        this.movies = result.data;
      });
  }

  ngOnInit() {
  }

  onDetails(movie) {
    // alert('showing the details of : ' + movie.title);
    this.router.navigate(['/movie-details'], { queryParams: { id: movie.id } });
  }

  onDelete(movie) {
    const answer = confirm('Are you sure you want to delete ' +  movie.title + ' ?');
    if (answer) {
      this.movieService
        .deleteMovie(movie.id)
        .subscribe(response => {
          const result = response.json();
          console.log(result);
          this.refreshMovieList();
        });
    }
  }

}
