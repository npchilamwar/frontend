import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  Employees=[];
  password ='';
  empFirstName ='';
  empLastName =''; 
  role ='';
  address =''; 
  empUID =''; 
  dateOfJoining =''; 
  mobileNo ='';
  emailId ='';

  constructor(
    private router: Router,
    private employeeService: EmployeeService) { }

  ngOnInit() {
  }

  
  onAdd() {

    this.employeeService
      .addEmployee(this.password, this.empFirstName, this.empLastName, this.role, this.address, this.empUID, this.dateOfJoining, this.mobileNo, this.emailId)
      .subscribe(response => {
        console.log(response);
        // /movie-list

        // go to the list of movies
        this.router.navigate(['/list-employee']);
      });
  }

  onCancel() {
    this.router.navigate(['/list-employee']);
  }

}
