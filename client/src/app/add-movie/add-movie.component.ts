import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {
  title = '';
  year = '';
  directors = '';
  writers = '';
  stars = '';
  genre = '';
  shortDescription = '';
  storyline = '';

  constructor(
    private router: Router,
    private movieService: MovieService) { }

  ngOnInit() {
  }

  
  onAdd() {

    this.movieService
      .addMovie(this.title, this.year, this.directors, this.writers, this.stars, this.genre, this.shortDescription, this.storyline)
      .subscribe(response => {
        console.log(response);
        // /movie-list

        // go to the list of movies
        this.router.navigate(['/movie-list']);
      });
  }

  onCancel() {
    this.router.navigate(['/movie-list']);
  }

}
