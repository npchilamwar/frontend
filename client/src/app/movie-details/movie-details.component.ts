import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {
  selectedMovie = {};

  constructor(
    private movieService: MovieService,
    private activedRoute: ActivatedRoute) {

    this.activedRoute.queryParams.subscribe(params => {
      const id = params['id'];
      // console.log(`selected movie id: ${id}`);
      this.movieService
        .getMovieDetails(id)
        .subscribe(response => {
          const result = response.json();
          this.selectedMovie = result.data;
        });
    });
  }

  ngOnInit() {
  }

}
