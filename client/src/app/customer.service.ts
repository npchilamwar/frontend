import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CustomerService {

  url = 'http://localhost:3000/customer';

  constructor(private http: Http) {

  }

  getCustomer() {
    return this.http.get(this.url);
  }

  getCustomerDetails(custId: number) {
    return this.http.get(this.url + '/' + custId);
  }

  addCustomer(custFirstName: string, custLastName: string, custMobile: string,
      custEmail: string, custPan: string) {

      const body = {
        custFirstName : custFirstName,
        custLastName : custLastName,
        custMobile : custMobile,
        custEmail :custEmail,
        custPan : custPan
      };

      const header = new Headers({ 'Content-Type': 'application/json' });
      const requestOption = new RequestOptions({ headers: header });

      return this.http.post(this.url, body, requestOption);
  }

  updateMovie() {

  }

  deleteDelete(custId: number) {
    return this.http.delete(this.url + '/' + custId);
  }

}
