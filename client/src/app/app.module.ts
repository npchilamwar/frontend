import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { MovieListComponent } from './movie-list/movie-list.component';
import { MovieService } from './movie.service';
import {EmployeeService} from './employee.service';

import { RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { HelpComponent } from './help/help.component';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { LoginComponent } from './login/login.component';
import { UserService } from './user.service';
import { ListEmployeeComponent } from './list-employee/list-employee.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { CustomerService } from './customer.service';
import { ListCustomerComponent } from './list-customer/list-customer.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { FeedbackService } from './feedback.service';
import { ShowInventoryComponent } from './show-inventory/show-inventory.component';
import { ShowInventoryService } from './show-inventory.service';


@NgModule({
  declarations: [
    AppComponent,
    AddMovieComponent,
    MovieListComponent,
    AboutComponent,
    HelpComponent,
    MovieDetailsComponent,
    LoginComponent,
    ListEmployeeComponent,
    AddEmployeeComponent,
    EmployeeDetailsComponent,
    AddCustomerComponent,
    ListCustomerComponent,
    WelcomeComponent,
    FeedbackComponent,
    ShowInventoryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: 'movie-add', component: AddMovieComponent, canActivate: [UserService] },
      { path: 'movie-list', component: MovieListComponent },
      { path: 'about', component: AboutComponent },
      { path: 'help', component: HelpComponent },
      { path: 'movie-details', component: MovieDetailsComponent },
      { path: 'login', component: LoginComponent },
      { path: 'list-employee', component: ListEmployeeComponent },
      { path: 'employee-details', component: EmployeeDetailsComponent },
      { path: 'add-employee', component: AddEmployeeComponent },
      { path: 'add-customer', component: AddCustomerComponent },
      { path: 'list-customer', component: ListCustomerComponent},
      { path: 'feedback', component: FeedbackComponent}, 
      { path: 'show-inventory', component: ShowInventoryComponent} 





    ])
  ],
  providers: [
    MovieService,
    UserService,
    EmployeeService,
    CustomerService,
    FeedbackService,
    ShowInventoryService
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
