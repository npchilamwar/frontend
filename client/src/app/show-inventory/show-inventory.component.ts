import { Component, OnInit } from '@angular/core';
import { ShowInventoryService } from '../show-inventory.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-show-inventory',
  templateUrl: './show-inventory.component.html',
  styleUrls: ['./show-inventory.component.css']
})
export class ShowInventoryComponent implements OnInit {
Inventory:any;
tempId:number;
service:ShowInventoryService;
isVisible:number=0;
productName:string;
vendorId:any;
vendorName:string;
pricePerItem:any;
productStock:any;
productTax:any;
updateDate:string;
  constructor(service:ShowInventoryService,private router:Router) 
  {
    this.service=service;
   this.reloadAllInventory(); 
   }
getForm(type)
{
this.isVisible=type;
}
   reloadAllInventory()
   {
    this.service.showAllInventory().subscribe((response)=>{
      var result=response.json();
      if(result!=null)
      {
 //       alert("allInventory result received");
        this.Inventory=result;  
      }
      else
      {
        alert("something went wrong");
      }
    });
   }
   updateInventoryShowItem(productId)
      {
        this.tempId=productId;
       alert("updateInventory called"+this.tempId); 
      //this.service.updateInventoryItem(productId);
        this.Inventory.forEach(inventory => {
          if(inventory.productId==productId)
          {
            console.log(inventory.productId); 
          console.log (inventory.productName);
          console.log(inventory.vendorId);
          console.log(inventory.vendorName);
          console.log(inventory.pricePerItem);
          console.log(inventory.productStock);
          console.log(inventory.productTax); 
          console.log(inventory.updateDate);
         this.productName=inventory.productName;
          this.vendorId=inventory.vendorId ;
          this.vendorName=inventory.vendorName;
          this.pricePerItem=inventory.pricePerItem;
          this.productStock=inventory.productStock;
          this.productTax=inventory.productTax;
          this.updateDate=inventory.updateDate;
          
          }
          

          
        });
      }
      updateInventoryItem()
      {
        alert("finally update called"+this.tempId);
        this.service.updateInventoryItem(this.tempId,this.productName,this.vendorId,this.vendorName,this.pricePerItem,this.productStock,this.productTax,this.updateDate).subscribe((response)=>{
          var result=response.json();
          if(result!=null)
          {
            alert("successfully updated");
            this.reloadAllInventory();
            this.isVisible=0;
          }
          else
          {
            alert("something went wrong");
          }
        })
      }
      deleteInventoryItem(productId)
      {
        alert("deleteinventorycalled"+productId);
      this.service.deleteinventoryItem(productId).subscribe((response)=>{
        console.log("response");
        this.reloadAllInventory();
      })
      }

      addInventoryItem()
      {
       // alert("add New Item is called");
        this.service.addNewInventoryItem(this.productName,this.vendorId,this.vendorName,this.pricePerItem,this.productStock,this.productTax,this.updateDate).subscribe((response)=>{
          var result=response.json();
          if(result!=null)
          {
            alert("your item has been successfully added");
            this.reloadAllInventory();
            this.isVisible=0;
          }
          else
          {
            alert("something went wrong");
          }
        });
      }

  ngOnInit() {
  }

}