import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Router } from '@angular/router';
@Component({
  selector: 'list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.css']
})
export class ListEmployeeComponent implements OnInit {
  Employees=[];
  password ='';
  empFirstName ='';
  empLastName =''; 
  role ='';
  address =''; 
  empUID =''; 
  dateOfJoining =''; 
  mobileNo =''; 
  emailId ='';

  constructor(
    private router: Router,
    private employeeService: EmployeeService) {
      this.refreshEmployeeList();
     }


  ngOnInit() {
  }

  refreshEmployeeList() {
    this.employeeService
      .getEmployee()
      .subscribe(response => {
        const result = response.json();
        console.log(result);
        this.Employees = result.data;
      });
  }
  onAdd() {

    this.employeeService
      .addEmployee(this.password, this.empFirstName, this.empLastName, this.role, this.address, this.empUID, this.dateOfJoining, this.mobileNo, this.emailId)
      .subscribe(response => {
        console.log(response);
        this.router.navigate(['/list-employee']);
      });
  }
  onDetails(employee) {
    this.router.navigate(['/employee-details'], { queryParams: { employeeId: employee.employeeId } });
  }

  onDelete(employee) {
    const answer = confirm('Are you sure you want to delete ' +  employee.empFirstName + employee.empLastName + ' ?');
    if (answer) {
      this.employeeService
        .deleteEmployee(employee.employeeId)
        .subscribe(response => {
          const result = response.json();
          console.log(result);
          this.refreshEmployeeList();
        });
    }
  }

  onCancel() {
    this.router.navigate(['/list-employee']);
  }
}
