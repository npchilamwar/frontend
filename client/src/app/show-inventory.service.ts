import { Injectable } from '@angular/core';
import {Http,RequestOptions,Headers} from '@angular/http';
@Injectable()
export class ShowInventoryService {
url:string="http://localhost:3000/inventory";
http:Http;
urlUpdate:string;
urlDelete:string;
    constructor(http:Http)
     {
         this.http=http;
      }
      showAllInventory()
      {
          return this.http.get(this.url);
      }
      updateInventoryItem(productId,productName,vendorId,vendorName,pricePerItem,productStock,productTax,updateDate)
      {
        this.urlUpdate=this.url+"/"+productId;
        alert("this is service update"+this.urlUpdate);
        var body={
            "productName":productName,
            "vendorId":vendorId,
            "vendorName":vendorName,
            "pricePerItem":pricePerItem,
            "productStock":productStock,
            "productTax":productTax,
            "updateDate":updateDate
        };
        var header=new Headers({'Content-type':'application/json'});
        var requestOption=new RequestOptions({headers:header});
        return this.http.put(this.urlUpdate,body,requestOption);
    }
      deleteinventoryItem(productId)
      {
          this.urlDelete=this.url+"/"+productId;
          alert(this.urlDelete);
          return this.http.delete(this.urlDelete);
      }
      addNewInventoryItem(productName,vendorId,vendorName,pricePerItem,productStock,productTax,updateDate)
      {
      var body={
        "productName":productName,
        "vendorId":vendorId,
        "vendorName":vendorName,
        "pricePerItem":pricePerItem,
        "productStock":productStock,
        "productTax":productTax,
        "updateDate":updateDate
        };
        var header=new Headers({'Content-type':'application/json'});
        var requestOption=new RequestOptions({headers:header});
        return this.http.post(this.url,body,requestOption);

      }
}